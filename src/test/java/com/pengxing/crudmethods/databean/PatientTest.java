package com.pengxing.crudmethods.databean;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by jean on 23/06/17.
 */
public class PatientTest {

    private MockMvc mockMvc;

    @MockBean
    private Patient patient;

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Patient
     */
    int id = 1;
    String lastName = "Last Name";
    String firstName = "First Name";
    String diagnosis = "Diagnosis";
    Calendar admissionDate = Calendar.getInstance();
    Calendar releaseDate = Calendar.getInstance();

    /**
     * Inpatiente
     */
    int patientId = 1;
    Calendar dateOfStay = Calendar.getInstance();
    String roomNumber = "123";
    double dailyRate = 1D;
    double supplies = 1D;
    double services = 1D;

    /**
     * Surgical
     */
    Calendar dateOfSurgery = Calendar.getInstance();
    String surgery = "Surgery";
    double roomFee = 1D;
    double surgeOnFee = 1D;

    /**
     * Medication
     */
    Calendar dateOfMed = Calendar.getInstance();
    String med = "Med";
    double unitCost = 1D;
    double units = 1D;


    @Test
    public void createPatienteTest() throws Exception {

        Patient patient = createNewPatiente();

        Mockito.when(patient).thenReturn(patient);

    }

    private Patient createNewPatiente() {

        Patient patient = new Patient();
        patient.setPATIENTID(id);
        patient.setLASTNAME(lastName);
        patient.setFIRSTNAME(firstName);
        patient.setDIAGNOSIS(diagnosis);
        patient.setADMISSIONDATE(new Timestamp(admissionDate.getTimeInMillis()));
        patient.setRELEASEDATE(new Timestamp(releaseDate.getTimeInMillis()));

        patient.setListOfInpatient(createNewImpatient());
        patient.setListOfSurgical(createNewSurgical());
        patient.setListOfMediacation(createNewMedication());

        return patient;
    }

    private List<Medication> createNewMedication() {
        Medication medication = new Medication();
        medication.setID(id);
        medication.setPATIENTID(patientId);
        medication.setDATEOFMED(new Timestamp(dateOfMed.getTimeInMillis()));
        medication.setMED(med);
        medication.setUNITCOST(unitCost);
        medication.setUNITS(units);

        ArrayList<Medication> medications = new ArrayList<>();
        medications.add(medication);

        return medications;
    }

    private List<Inpatient> createNewImpatient() {
        Inpatient inpatient = new Inpatient();
        inpatient.setID(id);
        inpatient.setPATIENTID(patientId);
        inpatient.setDateofstay(new Timestamp(dateOfStay.getTimeInMillis()));
        inpatient.setROOMNUMBER(roomNumber);
        inpatient.setDAILYRATE(dailyRate);
        inpatient.setSUPPLIES(supplies);
        inpatient.setSERVICES(services);
        inpatient.setListOfSurgical(createNewSurgical());

        ArrayList<Inpatient> inpatients = new ArrayList<>();
        inpatients.add(inpatient);

        return inpatients;
    }

    private List<Surgical> createNewSurgical() {
        Surgical surgical = new Surgical();
        surgical.setID(id);
        surgical.setPATIENTID(patientId);
        surgical.setDATEOFSURGERY(new Timestamp(dateOfSurgery.getTimeInMillis()));
        surgical.setSURGERY(surgery);
        surgical.setROOMFEE(roomFee);
        surgical.setSURGEONFEE(surgeOnFee);
        surgical.setSUPPLIES(supplies);

        ArrayList<Surgical> surgicals = new ArrayList<>();
        surgicals.add(surgical);

        return surgicals;
    }
}